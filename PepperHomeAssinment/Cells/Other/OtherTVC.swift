//
//  OtherTVC.swift
//  PepperHomeAssinment
//
//  Created by Sagi Marciano on 08/10/2021.
//

import UIKit

class OtherTVC: UITableViewCell {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
//    var delegate:OptionButtonsDelegate!
    @IBOutlet weak var infoBtn: UIButton!
    
    var operationId:Int?
    
    weak var delegate : OtherTVCDelegate?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.infoBtn.addTarget(self, action: #selector(infoBtnTapped(_:)), for: .touchUpInside)

    }
    
    @IBAction func infoBtnTapped(_ sender: UIButton){
        if let operationId = operationId,
           let delegate = delegate {
            self.delegate?.otherTVC(self, infoBtnTappedFor: operationId)
        }
      }
    
}

protocol OtherTVCDelegate: AnyObject {
  func otherTVC(_ otherTVC: OtherTVC, infoBtnTappedFor operationId: Int)
}
