//
//  chargeTVC.swift
//  PepperHomeAssinment
//
//  Created by Sagi Marciano on 08/10/2021.
//

import UIKit

class ChargeTVC: UITableViewCell {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var infoBtn: UIButton!
    
    var operationId:Int?
    
    weak var delegate : ChargeTVCDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.infoBtn.addTarget(self, action: #selector(infoBtnTapped(_:)), for: .touchUpInside)
    }
    
    @IBAction func infoBtnTapped(_ sender: UIButton){
        
        if let operationId = operationId,
           let delegate = delegate {
            self.delegate?.chargeTVC(self, infoBtnTappedFor: operationId)
        }
      }
    
}
protocol ChargeTVCDelegate: AnyObject {
  func chargeTVC(_ ctherTVC: ChargeTVC, infoBtnTappedFor operationId: Int)
}
