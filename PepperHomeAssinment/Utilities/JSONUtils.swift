//
//  JSONUtils.swift
//  PepperHomeAssinment
//
//  Created by Sagi Marciano on 09/10/2021.
//

import Foundation

func readLocalJSONFile(forName name: String) -> Data? {
    do {
        if let filePath = Bundle.main.path(forResource: name, ofType: "json") {
            let fileUrl = URL(fileURLWithPath: filePath)
            let data = try Data(contentsOf: fileUrl)
            return data
        }
    } catch {
        print("error: \(error)")
    }
    return nil
}


/// Parse the jsonData using the JSONDecoder with help of sampleRecord model
/// - Parameter jsonData: jsonData object
func parse(jsonData: Data) -> OperationsModel? {
    do {
        let decodedData = try JSONDecoder().decode(OperationsModel.self, from: jsonData)
        return decodedData
    } catch {
        print("error: \(error)")
    }
    return nil
}
