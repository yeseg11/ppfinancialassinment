//
//  ViewController.swift
//  PepperHomeAssinment
//
//  Created by Sagi Marciano on 08/10/2021.
//

import UIKit




class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate {
    
    
    
    var selectedOperation:OperationModel?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var mainTV: UITableView!
    
    var operations : [OperationModel] = []
    
    var filterOperations = [OperationModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpData()
        setUpSearchBar()
        searchBar.layer.borderWidth = 0
        searchBar.searchBarStyle = .minimal
        searchBar.layer.borderColor = UIColor.clear.cgColor
        self.mainTV.tableHeaderView = searchBar
        mainTV.delegate = self
        mainTV.dataSource = self
        mainTV.register(UINib(nibName: "cashTVC", bundle: nil), forCellReuseIdentifier: "cashTVC")
        mainTV.register(UINib(nibName: "ChargeTVC", bundle: nil), forCellReuseIdentifier: "ChargeTVC")
        mainTV.register(UINib(nibName: "OtherTVC", bundle: nil), forCellReuseIdentifier: "OtherTVC")
        
        
    }
    
    private func setUpData(){
        let jsonData = readLocalJSONFile(forName: "operationData")
        if let data = jsonData {
            if let operationObj = parse(jsonData: data) {
                let operationList: OperationsModel = operationObj
                operations = operationList.operations
                filterOperations = operations
            }
        }
        
    }
    
    private func setUpSearchBar(){
        searchBar.delegate = self
        
    }
    
    //Search Bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            filterOperations = operations
            mainTV.reloadData()
            return }
        filterOperations = self.operations.filter({ (operation) -> Bool in
            return operation.operationDesc?.range(of: searchText, options: [ .caseInsensitive, .diacriticInsensitive ]) != nil ||
                  operation.operationType.range(of: searchText, options: [ .caseInsensitive, .diacriticInsensitive ]) != nil ||
                  String(operation.operationId).range(of: searchText, options: [ .caseInsensitive, .diacriticInsensitive ]) != nil ||
                  operation.source?.range(of: searchText, options: [ .caseInsensitive, .diacriticInsensitive ]) != nil ||
                  String(operation.amount).range(of: searchText, options: [ .caseInsensitive, .diacriticInsensitive ]) != nil ||
                  operation.address?.range(of: searchText, options: [ .caseInsensitive, .diacriticInsensitive ]) != nil
        })
        
        mainTV.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        filterOperations = operations
        mainTV.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterOperations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellData = filterOperations[indexPath.row]
        switch cellData.operationType {
            case "CASH_WITHDRAWAL":
                let cell = tableView.dequeueReusableCell(withIdentifier: "cashTVC", for: indexPath) as! cashTVC
                cell.titleLbl.text = cellData.source
                cell.subTitleLbl.text = cellData.address
                cell.amountLbl.text = String(cellData.amount) + "$"
                return cell
            case "CHARGE":
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChargeTVC", for: indexPath) as! ChargeTVC
                cell.titleLbl.text = cellData.operationDesc
                cell.amountLbl.text =  String(cellData.amount) + "$"
                cell.operationId = cellData.operationId
                cell.delegate = self
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherTVC", for: indexPath) as! OtherTVC
                cell.titleLbl.text = cellData.operationDesc
                cell.amountLbl.text = String(cellData.amount) + "$"
                cell.operationId = cellData.operationId
                cell.delegate = self
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filterOperations[indexPath.row].operationType == "CASH_WITHDRAWAL"{
            selectedOperation = filterOperations[indexPath.row]
            performSegue(withIdentifier: "operationSegue", sender: nil)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch filterOperations[indexPath.row].operationType {
            case "CASH_WITHDRAWAL":
                return 60
            case "CHARGE":
                return 50
            default:
                return 125
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "operationSegue",let operationVC = segue.destination as? OperationVC{
            operationVC.operationId = selectedOperation?.operationId ?? 0 
        }
    }
    
    
  
}
extension ViewController : OtherTVCDelegate {
    func otherTVC(_ otherTVC: OtherTVC, infoBtnTappedFor operationId: Int) {
        let vc = UIStoryboard.init(name: "OperationSB", bundle: nil).instantiateInitialViewController() as! OperationVC
         vc.operationId = operationId
         self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController : ChargeTVCDelegate {
    func chargeTVC(_ ctherTVC: ChargeTVC, infoBtnTappedFor operationId: Int) {
        let vc = UIStoryboard.init(name: "OperationSB", bundle: nil).instantiateInitialViewController() as! OperationVC
         vc.operationId = operationId
         self.navigationController?.pushViewController(vc, animated: true)
    }
}
