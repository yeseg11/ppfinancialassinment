//
//  operationVC.swift
//  PepperHomeAssinment
//
//  Created by Sagi Marciano on 08/10/2021.
//

import UIKit

class OperationVC: UIViewController {

    var operationId:Int?
    
    
    @IBOutlet weak var operationIdLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        operationIdLbl.text = String(operationId ?? 0)
        // Do any additional setup after loading the view.
    }
    

}
