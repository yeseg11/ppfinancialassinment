//
//  operationModel.swift
//  PepperHomeAssinment
//
//  Created by Sagi Marciano on 09/10/2021.
//

import Foundation

struct OperationsModel: Codable{
    let operations: [OperationModel] //List of Operations
}

// MARK: - SuperHeroModel
struct OperationModel: Codable, Hashable {
//    static func == (lhs: OperationModel, rhs: OperationModel) -> Bool {
//        return lhs.operationType == rhs.operationType && lhs.amount == rhs.amount && lhs.source == rhs.source && lhs.address == rhs.address && lhs.operationId == rhs.operationId
//    }
    let operationId: Int
    let operationType: String
    let operationDesc: String?
    let amount: Double
    let source: String?
    let address: String?
    
}
